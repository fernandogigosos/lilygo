# lilygo

Código arduino para ejecutarse en la tarjeta TTGO T5 V2.3 ESP32


Es necesario para la ejecución de este código las librerias de arduino IDE: "Adafruit_GFX y GxEPD"
Despues de haber obtenido las mencionadas librerias los contenidos de la carpeta libraries deben trasladarse a /Documentos/Arduino/Libraries 

Si se desea realizar una modificación de las librerias de manipulación fina del display, se deben cargar en ese Path, si no el cambio 
queda registrado en el repositorio pero no toma efecto
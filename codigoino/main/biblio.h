#define LILYGO_T5_V213

//imagenes


#include "BluetoothSerial.h"
#include "data/defaultQR.h"
#include "data/nuEVA.h"
#include "data/default.h"
#include "data/QR.h"
#include "data/bat0.h"
#include "data/bat1.h"
#include "data/bat2.h"
#include "data/bat3.h"
#include "data/bat4.h"

//cosas de la pantalla

#include <Adafruit_GFX.h>
#include <boards.h>
#include <GxEPD.h>
#include <GxGDEM0213B74/GxGDEM0213B74.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

#include GxEPD_BitmapExamples

//cosas del reloj

#define PERIOD (60*1000L)
#include <ESP32Time.h>

//para leer el voltaje de la batería

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#include <Pangodream_18650_CL.h>
#define MIN_USB_VOL 4.9
#define ADC_PIN 34
#define CONV_FACTOR 1.8
#define READS 20

// FreeFonts from Adafruit_GFX

#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold24pt7b.h>

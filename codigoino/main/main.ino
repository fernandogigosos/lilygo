//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Evandro Copercini - 2018
//
//This example creates a bridge between Serial and Classical Bluetooth (SPP)
//and also demonstrate that SerialBT have the same functionalities of a normal Serial

#include "biblio.h"

//creación del handler de la pantalla
GxIO_Class io(SPI,  EPD_CS, EPD_DC,  EPD_RSET);
GxEPD_Class display(io, EPD_RSET, EPD_BUSY);

//variables para el reloj
ESP32Time rtc;
String minuto;
bool tiempoACK = false;
unsigned long target_time = 0L;

//creación del handler de la bateria
Pangodream_18650_CL BL(ADC_PIN, CONV_FACTOR, READS);
const uint8_t *baterias[] = {gImage_bat0, gImage_bat1, gImage_bat2, gImage_bat3, gImage_bat4};

//pines
const int LEDPIN = 22; 
const int PushButton1 = 15;

//bluetooth
#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;
bool primera = true;

void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT && primera){
   escribirPantalla("Conectado a algo");
   primera = false;
  }
}

void setup() {
  Serial.begin(115200);
  display.init();
  SerialBT.register_callback(callback);

  //setup del reloj a 0, esperamos datos del maestro
  rtc.setTime(0);
  minuto = "0";

  //entrada de los pines
  pinMode(LEDPIN, OUTPUT);
  pinMode(PushButton1, INPUT);
  /**
  pinMode(14, OUTPUT);
  digitalWrite(14, HIGH);
**/

  
  Serial.println("setup");

  
  if(!SerialBT.begin("ESP32test")){ //Bluetooth device name
    Serial.println("ha ocurrido un error inicializando el BT");
  }else{
    Serial.println("The device started, now you can pair it with bluetooth!");
  }
  display.fillScreen(GxEPD_WHITE);
  display.drawExampleBitmap(BitmapExample2, sizeof(BitmapExample1), GxEPD::bm_default | GxEPD::bm_partial_update);
  delay(500);
  display.fillScreen(GxEPD_WHITE);
  display.update();
  escribirPantalla("ESP32test visible");
  display.fillRect(0, display.height(), display.width(), display.height(), GxEPD_WHITE);

  //xTaskCreate(battery_info, "battery_info", 2048, NULL, 1, NULL);
}

void loop() {
  //APLICAR MULTITHREADING PARA ESTO
  if(millis() - target_time >= PERIOD && tiempoACK){

        target_time += PERIOD;
        minuto = rtc.getTime("%M");
        mostrarTiempo();
  }

  //comprueba el boton a ver que dice
  if ( digitalRead(PushButton1) == HIGH ){ 
    digitalWrite(LEDPIN, HIGH); 
    SerialBT.println("se ha pulsado el botón");
    mostrarImagen(1);
    
    digitalWrite(LEDPIN, LOW); 
  }

  //lectura mini-usb
  if (Serial.available()) {
    SerialBT.write(Serial.read());
  }
  if (SerialBT.available()) {
    static char mens[1024];
    leerBT(mens, sizeof(mens));
    
    if (!strcmp("imagen", mens)) {
      escribirPantalla("mostrando imagen");
      mostrarImagen(1);
    }
    else if(!strcmp("imagendefault", mens)) {
      escribirPantalla("mostrando por defecto");
      mostrarImagen(2);
    }
    else if(!strcmp("QR", mens)) {
      mostrarImagen(3);
    }
    else if(!strcmp("imagenfija", mens)) {
      escribirPantalla("mostrando imagen fija");
      mostrarImagenFija();
    } else {
    escribirPantalla(mens, 3, 83, 244, 21);
    //escribirPantalla(lectura);
    }
  }

}

int leerBT(char* lectura, size_t sz){
  sz--;
  int i = 0;
  for(; SerialBT.available() && i < sz; i++){   
      lectura[i] = SerialBT.read();
      Serial.write(lectura[i]);     
  }
  lectura[i] = 0;
  
  return i;
}

void escribirPantalla(char* mensaje, uint16_t x, uint16_t y, uint16_t w, uint16_t h){

    Serial.printf("-display: %s\n" ,mensaje);
    display.setRotation(1);
    //display.fillRect(0, display.height()/2 - 20, display.width(), display.height()/4, GxEPD_WHITE);
    display.fillRect(x, y, w, h, GxEPD_WHITE);
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold9pt7b);
    //display.setCursor(0, display.height()/2);
    display.setCursor(x+1, y+12);
    

    display.print(mensaje);
    //display.updateWindow(0, display.height()/2, display.width(), display.height()/4, true);
    display.updateWindow(x, y, w, h, true);
}

void escribirPantalla(char* mensaje){

    display.setRotation(1);
    display.fillRect(0, 0, display.width(), display.height(), GxEPD_WHITE);
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold9pt7b);
    display.setCursor(0, display.height()/2);
    
    display.print(mensaje);
    display.updateWindow(0, 0, display.width(), display.height(), true);
    //display.fillRect(0, 0, display.width(), display.height(), GxEPD_WHITE);
}

void mostrarTiempo(){
    display.setRotation(1);
    display.fillRect(43, 25, 55, 16, GxEPD_WHITE);
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold9pt7b);
    display.setCursor(42, 39);
    
    display.print(rtc.getTime("%H:%M"));
    display.updateWindow(42, 31, 60, 16, true);

}

void mostrarImagen(uint16_t tipo){
    //Serial.println(sizeof(gImage_UVA));
    switch(tipo){
      case 1:
        Serial.println("entro");
        display.drawBitmap(gImage_defaultQR, 0, 0, 250, 128, GxEPD::bm_normal);             
        Serial.println("imagen cargada");
        mostrarBateria(baterias[4], false);
        Serial.println("batería puesta");
        display.updateWindow(0, 0, display.width(), display.height(), true); 
        Serial.println("refrescada");
        break;
      case 2:
        display.drawBitmap(gImage_default, 4000, GxEPD::bm_default | GxEPD::bm_partial_update);
        break;
      case 3: 
        display.drawBitmap(gImage_QR, 167, 0, 83, 83, GxEPD_WHITE, GxEPD::bm_normal);
        display.updateWindow(167, 0, 83, 83, true);
        break;
    }
    return;
}
void mostrarImagenFija(){
    //Serial.println(sizeof(gImage_UVA));
    display.drawExampleBitmap(gImage_nuEVA, 4000);
    
}

void mostrarBateria(const uint8_t *bitmap, bool refrescar){
   display.drawBitmap(bitmap, 132, 26, 32, 14, GxEPD_WHITE, GxEPD::bm_normal);
   if(refrescar){
      display.updateWindow(132, 32, 32, 14, true);
   }
}

void battery_info(void *arg){
  while (1) {
    //String mensajitos = (String)BL.pinRead()+ String(" V: ") + (String)BL.getBatteryVolts() +  (String)BL.getBatteryChargeLevel();
    //escribirPantalla(mensajitos);

    if(BL.getBatteryVolts() >= MIN_USB_VOL){
      for(int i=0; i< ARRAY_SIZE(baterias); i++){
        mostrarBateria(baterias[i], true);
        vTaskDelay(500);
      }
    }else{
        int imgNum = 0;
        int batteryLevel = BL.getBatteryChargeLevel();
        if(batteryLevel >=80){
          imgNum = 3;
        }else if(batteryLevel < 80 && batteryLevel >= 50 ){
          imgNum = 2;
        }else if(batteryLevel < 50 && batteryLevel >= 20 ){
          imgNum = 1;
        }else if(batteryLevel < 20 ){
          imgNum = 0;
        }  
    
        mostrarBateria(baterias[imgNum], true);    
        vTaskDelay(1000);
    }
  }
}

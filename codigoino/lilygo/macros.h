//TIPOS Y MACROS

#define SIZE_BUF  264 //cadena transmitida mas el \0 
#define SIZE_MENS 256
#define NUM_MENS  64

#define RTS  5 //ENQUIRY
#define CTS  6 //ACKNOWLEDGE
#define EOT  4 //END OF TRANSMISSION
#define BELL 7 //BELL (HE RECIBIDO Y GUARDADO)

#define ESPERA 200

#define LEDROJO 21
#define LEDVERDE 22

#define BOTON1 14
#define BOTON2 13
#define BOTON4 15
#define BOTON3 2



struct mensaje {
  char clave;
  char texto[SIZE_MENS];
  int  longitud;
};

struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};

struct Vista{
  char* texto1 = NULL;
  char* texto2 = NULL;
  char* texto3 = NULL;
  char* texto4 = NULL;
  char* texto5 = NULL;
  char* texto6 = NULL;

  int subVista = -1;
  void  (*funShw)();
  Vista (*funSig)();
  Vista (*funAnt)();
  Vista (*funSel)();
  Vista (*menu)();
};

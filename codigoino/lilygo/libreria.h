#define LILYGO_T5_V213

#include "data/defaultQR.h"
#include "data/default.h"
#include "data/prueba.h"
#include "data/INICIO.h"
#include "data/MENU.h"
#include "data/MENU1.h"
#include "data/MENU2.h"
#include "data/MENU3.h"
#include "data/MENU4.h"
#include "data/no_dis.h"
#include "data/llamando.h"
#include "data/error_llamada.h"
#include "data/HORARIO.h"
#include "data/TUTORIA.h"
#include "data/ASIGNATURAS.h"
#include "data/bat0.h"
#include "data/bat1.h"
#include "data/bat2.h"
#include "data/bat3.h"
#include "data/bat4.h"

#include <string.h>
#include <Adafruit_GFX.h>
#include <boards.h>


#include <GxGDEM0213B74/GxGDEM0213B74.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

// FreeFonts from Adafruit_GFX

#include <Fonts/FreeMonoBold9pt7b.h>
#include "data/Courier10PBold.h"

#include "BluetoothSerial.h"

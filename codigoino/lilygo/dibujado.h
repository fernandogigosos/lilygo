void escribirPantalla(char* original, uint16_t x, uint16_t y, uint16_t w, uint16_t h){
    char* mensaje;
    mensaje = (char*)malloc(strlen(original));
    strcpy(mensaje, original);
    int lines    = h/12;
    int columns  = w/10;
    if(lines == 0) return;    
    display.fillRect(x, y, w, h, GxEPD_WHITE);
    char lineas[lines];
    char c , cc;
    int l;
      for(l = 0; l < lines; l++){
        display.setCursor(x+1, y+12+12*l);
        if(strlen(mensaje) <= columns) break;
        if(mensaje[columns] == ' '){       
          mensaje[columns] = 0;           
          display.print(mensaje);          
          mensaje = &mensaje[columns+1];
        }else{
          if(mensaje[columns-1] == ' '){
            mensaje[columns-1] = 0;           
            display.print(mensaje);          
            mensaje = &mensaje[columns];
          }else{
            cc = mensaje[columns-1];
            mensaje[columns-1] = '-';
            c = mensaje[columns];
            mensaje[columns] = 0; 
            display.print(mensaje);
            mensaje[columns-1] = cc;
            mensaje[columns] = c;
            mensaje = &mensaje[columns-1];
          }
        }     
      }
      if(l < lines)
      display.print(mensaje);
}

#ifndef GFXLATIN1_H
#define GFXLATIN1_H


// Convert a UTF-8 encoded string to a modified ISO 8859-1 encoded
// string compatible with the GFX Latin-1 character set.
// Be careful, the in-situ conversion will "destroy" the UTF-8
// string s.
void utf8tocp(char* s);    

#endif

// https://playground.arduino.cc/Main/Utf8ascii/
// ****** UTF8-Decoder: convert UTF8-string to extended ASCII *******

byte c1 = 0;  // Last character buffer

// Convert a single glyph from UTF8 to Extended ASCII
// Return "0" if a byte has to be ignored
byte utf8_to_xtd_ascii(byte ascii) {

    if (ascii < 128)  { // Standard ASCII-set 0..0x7F handling  
      c1 = 0;
      if (ascii < 32)
        return 0;
      else  
        return ascii;
    }

    byte last = c1;
    c1 = ascii;

    if (last == 0xC2) {
      return ascii - 32;
    } else if (last == 0xC3) {
      return ascii + 32; 
    }
    return 0;
}  

// Convert String object from UTF8 string to extended ASCII
String utf8tocp(String s) {     
  String r="";
  char c;
  c1 = 0;
  for (int i=0; i<s.length(); i++) {
    c = utf8_to_xtd_ascii(s.charAt(i));
    if (c!=0) 
      r += c;
  }
  return r;
}

// In place conversion of a UTF8 string to extended ASCII string (ASCII is shorter!)
void utf8tocp(char* s) {      
  int k = 0;
  char c;
  c1 = 0;
  for (int i=0; i<strlen(s); i++) {
    c = utf8_to_xtd_ascii(s[i]);
    if (c!=0) {
      s[k++] = c;
    }  
  }
  s[k]=0;
}

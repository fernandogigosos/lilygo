void vista3_shw();
void vista4_shw();
void vista5_shw();

/////////VISTA 1///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Vista vista1_sig(){
    return actual;
}


Vista vista1_ant(){
    return actual;
}

Vista vista1_sel(){  
  display.update();
  return actual;
}

Vista vista1_menu(){
   display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
   display.updateWindow(0, 0, display.width(), display.height(), true);
   vista2.subVista = -1;
   return vista2;
}
void vista1_shw(){
   if (vista1.texto1 == NULL){
      vista1.texto1 = strdup("SIN DATOS");
   }
   if (vista1.texto2 == NULL){
      vista1.texto2 = strdup("SIN DATOS");
   }
   if (vista1.texto3 == NULL){
      vista1.texto3 = strdup("SIN DATOS");
   }
   Serial.printf("se procesa la vista1:\nDatos:\n");
   Serial.printf("texto1 : %s\ntexto2 : %s\n", vista1.texto1, vista1.texto2);

   display.drawBitmap(gImage_default, 0, 0, 250, 128, GxEPD::bm_normal);
   escribirPantalla(vista1.texto1, 3, 43, 163, 37);
   escribirPantalla(vista1.texto2, 3, 83, 244, 36);
   escribirPantalla(vista1.texto3, 42, 27, 92, 15);
   display.updateWindow(0, 0, display.width(), display.height(), true);

}

/////////VISTA 2///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Vista vista2_sig(){
  int subVista = vista2.subVista;
  switch(subVista){
    case(-1):{
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 120, 40, true);
      vista2.subVista = 0;
      return vista2;
    }break;
    case(0):{
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU2, 127, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 1;
      return vista2;
    }break;
    case(1):{
      display.drawBitmap(gImage_MENU2, 127, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU3, 3, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 2;
      return vista2;
    }break;
    case(2):{
      display.drawBitmap(gImage_MENU3, 3, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 3;
      return vista2;
    }break;
    case(3):{
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 0;
      return vista2;
    }break;
    default:{
       return actual;
    } 
  }
}
Vista vista2_ant(){
  int subVista = vista2.subVista;
  switch(subVista){
    case(-1):{
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 3;
      return vista2;
    }break;
    case(0):{
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 3;
      return vista2;
    }break;
    case(1):{
      display.drawBitmap(gImage_MENU2, 127, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 0;
      return vista2;
    }break;
    case(2):{
      display.drawBitmap(gImage_MENU3, 3, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU2, 127, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 1;
      return vista2;
    }break;
    case(3):{
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU3, 3, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(3, 43, 244, 82, true);
      vista2.subVista = 2;
      return vista2;
    }break;
    default:{
       return actual;
    } 
  }
}
Vista vista2_sel(){
  int subVista = vista2.subVista;
  switch(subVista){
  case(-1):{
    display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
    display.updateWindow(3, 43, 120, 40, true);
    vista2.subVista = 0;
    return vista2;
  }break; 
  case(0):{
    if(vista2.texto1 != NULL){
      if(strcmp(vista2.texto1, "si") == 0){
        Serial.printf("horario seleccionado\n");
        vista3_shw();        
        return vista3;
      }
    }
      display.drawBitmap(gImage_no_dis, 59, 25, 139, 72, GxEPD::bm_normal);
      display.updateWindow(52, 19, 155, 87, true);
      display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU1, 3, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(0, 0, display.width(), display.height(), true);
      return vista2;      
  }break;
  case(1):{
    if(vista2.texto2 != NULL){
      if(strcmp(vista2.texto2, "si") == 0){
        Serial.printf("tutoría seleccionado\n");
        vista4_shw();
        return vista4;
      }
    }
      display.drawBitmap(gImage_no_dis, 59, 25, 139, 72, GxEPD::bm_normal);
      display.updateWindow(52, 19, 155, 87, true);
      display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU2, 127, 43, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(0, 0, display.width(), display.height(), true);
      return vista2;
  }break;
  case(2):{
    if(vista2.texto3 != NULL){
      if(strcmp(vista2.texto3, "si") == 0){
        Serial.printf("Asignaturas seleccionado\n");
        vista5_shw();
        return vista5;
      }
    }
      display.drawBitmap(gImage_no_dis, 59, 25, 139, 72, GxEPD::bm_normal);
      display.updateWindow(52, 19, 155, 87, true);
      display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU3, 3, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(0, 0, display.width(), display.height(), true);
      return vista2;
  }break;
  case(3):{
    if(vista2.texto4 != NULL){
      if(strcmp(vista2.texto4, "si") == 0){
        Serial.printf("Realizando Llamada\n");
        display.drawBitmap(gImage_llamando, 59, 25, 139, 72, GxEPD::bm_normal);
        display.updateWindow(52, 19, 155, 87, true);
        digitalWrite(LEDROJO, LOW);
        digitalWrite(LEDVERDE, HIGH);
        Saliente[0].clave = 'A';
        sprintf(Saliente[0].texto, "Llamada realizada");
        
        xSemaphoreTake(bMutex, portMAX_DELAY);
        while(!escribirBT(0) && intento < 5){
          Serial.println("no se ha envíado correctamente, reintentando...");
          intento++;
        }
        xSemaphoreGive(bMutex);
        digitalWrite(LEDVERDE, LOW);
        digitalWrite(LEDROJO, HIGH);
        
        
        if(intento < 5){
          intento = 0;
          vista1_shw();
          return vista1;
        }
        intento = 0;
        display.drawBitmap(gImage_error_llamada, 59, 25, 139, 72, GxEPD::bm_normal);
        display.updateWindow(52, 19, 155, 87, true);
        delay(100);
        display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
        display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
        display.updateWindow(0, 0, display.width(), display.height(), true);
        return vista2;
      }
    }
      display.drawBitmap(gImage_no_dis, 59, 25, 139, 72, GxEPD::bm_normal);
      display.updateWindow(52, 19, 155, 87, true);
      display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
      display.drawBitmap(gImage_MENU4, 127, 84, 120, 33, GxEPD_BLACK, GxEPD::bm_invert);
      display.updateWindow(0, 0, display.width(), display.height(), true);
      return vista2;
  }break;
  default:{return vista2;} 
  }
  return vista2;
}
Vista vista2_menu(){
    Serial.printf("volvemos a la vista 1\n");
    vista1_shw();
    return vista1;
}
void  vista2_shw(){

   Serial.printf("cargamos el menú básico\n");
   display.drawBitmap(gImage_MENU, 0, 0, 250, 128, GxEPD::bm_normal);
   display.updateWindow(0, 0, display.width(), display.height(), true);
}

///////////VISTA3/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Vista vista3_sig(){
  vista3.subVista = (++vista3.subVista) % 5;
  vista3_shw(); 
  return vista3;
}
Vista vista3_ant(){
  vista3.subVista = (vista3.subVista-1 < 0)? 4 : vista3.subVista-1;
  vista3_shw(); 
  return vista3;
}
Vista vista3_sel(){
  return vista3;
}
void vista3_shw(){
  int subVista = vista3.subVista;
  Serial.printf("cargamos el horario\n");
  display.drawBitmap(gImage_HORARIO, 0, 0, 250, 128, GxEPD::bm_normal);
  escribirPantalla(semana[subVista],144,21,90,17);
  for(int i=0; i<7; i++){
      escribirPantalla( (horario[subVista][i] == NULL? strdup(" "):horario[subVista][i]), 52, 8+(16*i) , 50, 12);
  }
  for(int i=7; i<12; i++){
      escribirPantalla( (horario[subVista][i] == NULL? strdup(" "):horario[subVista][i]), 176, 41+(16*(i-7)) , 50, 12);
  }
  display.updateWindow(0, 0, display.width(), display.height(), true);
}

//////////VISTA4////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Vista vista4_sig(){
  return vista4;
}
Vista vista4_ant(){
  vista2_shw();
  vista2.subVista = -1;
  return vista2;
}
Vista vista4_sel(){
  return vista4;
}
void vista4_shw(){
  display.drawBitmap(gImage_TUTORIA, 0, 0, 250, 128, GxEPD::bm_normal);
  escribirPantalla(vista4.texto1 == NULL? strdup(" "): vista4.texto1, 2, 83, 245, 37);
  display.updateWindow(0, 0, display.width(), display.height(), true);
}

/////////VISTA5////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Vista vista5_sig(){
  return vista5;
}
Vista vista5_ant(){
  vista2_shw();
  vista2.subVista = -1;
  return vista2;
}
Vista vista5_sel(){
  return vista5;
}
void vista5_shw(){
  display.drawBitmap(gImage_ASIGNATURAS, 0, 0, 250, 128, GxEPD::bm_normal);
  escribirPantalla(vista5.texto1 == NULL? strdup("SIN DATOS"): vista5.texto1, 3, 3, 128, 30);
  escribirPantalla(vista5.texto2 == NULL? strdup("NO HAY DATOS"): vista5.texto2, 3, 36, 244, 83);
  display.updateWindow(0, 0, display.width(), display.height(), true);
}

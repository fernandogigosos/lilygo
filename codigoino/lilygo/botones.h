//GESTOR DE BOTÓN

void IRAM_ATTR boton1Pulsado()
{
  if(!button1.pressed){
    button1.numberKeyPresses += 1;
    //digitalWrite(LEDVERDE, HIGH);
    Serial.printf("Boton1 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button1.numberKeyPresses);
    //intento = 0;
    button1.pressed = true;
  }
}
void IRAM_ATTR boton2Pulsado()
{
  if(!button2.pressed){   
    button2.numberKeyPresses += 1;
    Serial.printf("Boton2 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button2.numberKeyPresses);
    button2.pressed = true;      
  } 
}
void IRAM_ATTR boton3Pulsado()
{
  if(!button3.pressed){   
    button3.numberKeyPresses += 1;
    Serial.printf("Boton3 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button3.numberKeyPresses);
    button3.pressed = true;      
  } 
}
void IRAM_ATTR boton4Pulsado()
{
  if(!button4.pressed){   
    button4.numberKeyPresses += 1;
    Serial.printf("Boton4 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button4.numberKeyPresses);
    button4.pressed = true;      
  } 
}

void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT && primera){
   Serial.printf("Conectado a algo\n");
   primera = false;
  }
}

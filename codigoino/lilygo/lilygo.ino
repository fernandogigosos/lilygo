#include "libreria.h"
#include "macros.h"
#include "globales.h"

#include "transLatin.h"

#include "dibujado.h"
#include "comunicacion.h"
#include "funcionesVista.h"

#include "botones.h"


void setup()
{
  Serial.begin(115200);
  
  //BT CONFIGURATION
  SerialBT.register_callback(callback);
  
  if(!SerialBT.begin("ESP32test")){ //Bluetooth device name
    Serial.println("ha ocurrido un error inicializando el BT");
  }else{
    Serial.println("The device started, now you can pair it with bluetooth!");
  }
  

  //PIN CONFIG
  pinMode(button1.PIN, INPUT);

  pinMode(LEDROJO, OUTPUT);
  pinMode(LEDVERDE, OUTPUT);

  //GESTORES DE INTERRUPCIÓN
  attachInterrupt(button1.PIN, boton1Pulsado, RISING);
  attachInterrupt(button2.PIN, boton2Pulsado, RISING);
  attachInterrupt(button3.PIN, boton3Pulsado, RISING);
  attachInterrupt(button4.PIN, boton4Pulsado, RISING);

  //MANEJO DE HILOS
  xMutex = xSemaphoreCreateMutex();
  bMutex = xSemaphoreCreateMutex();
  
  xTaskCreatePinnedToCore(
      ProcesaMensaje,   /* Task function. */
      "ProcesaMensaje", /* name of task. */
      10000,            /* Stack size of task */
      NULL,             /* parameter of the task */
      5,                /* priority of the task */
      &procesa,         /* Task handle to keep track of created task */
      0);               /* pin task to core 0 */

  //SETUP DE LA PANTALLA
  display.init();
  display.update();
  display.setRotation(1);
  display.setTextColor(GxEPD_BLACK);
  //display.setFont(&FreeMonoBold9pt7b);
  display.setFont(&Courier10PitchBT_Bold9pt8b);
  display.drawBitmap(gImage_INICIO, 0, 0, 250, 128, GxEPD::bm_normal);
  display.updateWindow(0, 0, display.width(), display.height(), true);
  Serial.println("refrescada");
  
  //COLOCACIÓN DE LAS FUNCIONES EN SUS RESPECTIVAS VISTAS
  vista1.funSig = vista1_sig;
  vista1.funAnt = vista1_ant;
  vista1.funSel = vista1_sel;
  vista1.funShw = vista1_shw;
  vista1.menu   = vista1_menu;

  vista2.funSig = vista2_sig;
  vista2.funAnt = vista2_ant;
  vista2.funSel = vista2_sel;
  vista2.funShw = vista2_shw;
  vista2.menu   = vista2_menu;
  
  vista3.funSig = vista3_sig;
  vista3.funAnt = vista3_ant;
  vista3.funSel = vista3_sel;
  vista3.funShw = vista3_shw;
  vista3.menu   = vista1_menu;
  vista3.subVista = 0; //puede depender del día actual si metemos fecha y hora

  vista4.funSig = vista4_sig;
  vista4.funAnt = vista4_ant;
  vista4.funSel = vista4_sel;
  vista4.funShw = vista4_shw;
  vista4.menu   = vista1_menu;
  
  vista5.funSig = vista5_sig;
  vista5.funAnt = vista5_ant;
  vista5.funSel = vista5_sel;
  vista5.funShw = vista5_shw;
  vista5.menu   = vista1_menu;

  actual = vista1;
}

void loop()
{

  xSemaphoreTake(bMutex, portMAX_DELAY);
  if(button1.pressed && !boton1Procesando){
      boton1Procesando = true;
      xSemaphoreGive(bMutex); 
      xSemaphoreTake(xMutex, portMAX_DELAY);
      Mensaje[currMen].clave = 'm';
      currMen = (++currMen) % NUM_MENS;
      xSemaphoreGive(xMutex);
      button1.pressed = false;
  }else{button1.pressed = false;}
  xSemaphoreGive(bMutex);

  xSemaphoreTake(bMutex, portMAX_DELAY);
  if(button4.pressed && !boton4Procesando){
      boton4Procesando = true;
      xSemaphoreGive(bMutex); 
      xSemaphoreTake(xMutex, portMAX_DELAY);
      Mensaje[currMen].clave = 'w';
      currMen = (++currMen) % NUM_MENS;
      xSemaphoreGive(xMutex);
      button4.pressed = false;
  }else{button4.pressed = false;}
  xSemaphoreGive(bMutex);
    
  xSemaphoreTake(bMutex, portMAX_DELAY);
  if(button3.pressed && !boton3Procesando){
      boton3Procesando = true;
      xSemaphoreGive(bMutex); 
      xSemaphoreTake(xMutex, portMAX_DELAY);
      Mensaje[currMen].clave = 'v';
      currMen = (++currMen) % NUM_MENS;
      xSemaphoreGive(xMutex);
      button3.pressed = false;
  }else{button3.pressed = false;}
  xSemaphoreGive(bMutex);

  xSemaphoreTake(bMutex, portMAX_DELAY);
  if(button2.pressed && !boton2Procesando){
      boton2Procesando = true;
      xSemaphoreGive(bMutex); 
      xSemaphoreTake(xMutex, portMAX_DELAY);
      Mensaje[currMen].clave = 'u';
      currMen = (++currMen) % NUM_MENS;
      xSemaphoreGive(xMutex);
      button2.pressed = false;
  }else{button2.pressed = false;}
  xSemaphoreGive(bMutex);

  xSemaphoreTake(bMutex, portMAX_DELAY);
  if (SerialBT.available())
  {   
    int c;
    c = SerialBT.read();
    if (RTS == c)
    {      
      xSemaphoreTake(xMutex, portMAX_DELAY);
      Serial.printf("he recibido un %d : RTS\n", c);
      SerialBT.write(CTS);
      int sz = leerBT(currMen);
      if (sz < 0)
      {
        Serial.printf("Al leer se ha producido un error de tipo: %d\n", sz);
      }
      else
      {
        Serial.printf("lectura correcta\n");
        SerialBT.write(BELL);
        Serial.printf("he recibido:\n\tClave : %c\n\tTexto : %s\n\tTamaño de la cadena : %d\n", Mensaje[currMen].clave, Mensaje[currMen].texto, sz);
        utf8tocp(Mensaje[currMen].texto);
        currMen = (++currMen) % NUM_MENS;        
      }
      xSemaphoreGive(xMutex);
    }
    else
    {
      Serial.printf("he recibido un %d y no sé por qué\n", c);
    }
  }
  xSemaphoreGive(bMutex);

}

void ProcesaMensaje(void * pvParameters)
{
  Serial.print("ProcesaMensaje running on core ");
  Serial.println(xPortGetCoreID());

  for (;;)
  {
    vTaskDelay(1);
    xSemaphoreTake(xMutex, portMAX_DELAY);
    if (currMen != currLec)
    {
      xSemaphoreGive(xMutex);
      digitalWrite(LEDROJO, HIGH);      
      Serial.printf("procesado el mensaje %d, que contiene la clave %c\n", currLec, Mensaje[currLec].clave);
      switch (Mensaje[currLec].clave){
      case ('A'):{
        copiaCampo(vista1.texto1, currLec);
      }break;
      case ('B'):{
        copiaCampo(vista1.texto2, currLec);
      }break;
      case ('C'):{
        copiaCampo(vista1.texto3, currLec);
      }break;
      
      //activación de opciones del menú de vista2
      case ('D'):{
        copiaCampo(vista2.texto1, currLec);
      }break;
      case ('E'):{
        copiaCampo(vista2.texto2, currLec);
      }break;
      case ('F'):{
        copiaCampo(vista2.texto3, currLec);
      }break;
      case ('G'):{
        copiaCampo(vista2.texto4, currLec);        
      }break;

      //textos de vista3
      case ('H'):{
        procesaHorario(currLec);
      }break;

      //texto de vista 4
      case ('I'):{
        copiaCampo(vista4.texto1, currLec);
      }break;

      //texto de vista 5
      case ('J'):{
        copiaCampo(vista5.texto1, currLec);
      }break;
      case ('K'):{
        copiaCampo(vista5.texto2, currLec);
      }break;
      
      case ('z'):{
         actual.funShw();
      }break;
      //movimientos sobre la vista actual (sig, selec, anter)
      case ('w'):{        
        Serial.printf("siguiente\n");
        actual = actual.funSig();                  
        xSemaphoreTake(bMutex, portMAX_DELAY);      
        boton4Procesando = false;
        xSemaphoreGive(bMutex);
      }break;
      case ('v'):{
        Serial.printf("anterior\n");
        actual = actual.funAnt();
        xSemaphoreTake(bMutex, portMAX_DELAY); 
        boton3Procesando = false;
        xSemaphoreGive(bMutex);
      }break;
      case ('u'):{
        Serial.printf("seleccionar\n");
        actual = actual.funSel();
        xSemaphoreTake(bMutex, portMAX_DELAY);        
        boton2Procesando = false;
        xSemaphoreGive(bMutex);
      }break;
      case ('m'):{
        Serial.printf("menú\n");
        actual = actual.menu();
        xSemaphoreTake(bMutex, portMAX_DELAY);        
        boton1Procesando = false;
        xSemaphoreGive(bMutex);
      }break;
      
      case ('y'):{
        display.update();      
      }break;
      case ('x'):{
        display.drawBitmap(gImage_prueba, 0, 0, 250, 128, GxEPD::bm_normal);
        display.updateWindow(0, 0, display.width(), display.height(), true);
      }break;
      default:{
        Serial.printf("esto que es: %c\n", Mensaje[currLec].clave);
      }
      }
      currLec = (++currLec) % NUM_MENS;
    digitalWrite(LEDROJO, LOW);
    }
    xSemaphoreGive(xMutex);
  }
}

void copiaCampo(char* &hueco, int indice)
{
  if (hueco == NULL || (sizeof(hueco) <= Mensaje[indice].longitud))
  {
    hueco = (char *)malloc(Mensaje[indice].longitud);
    memcpy(hueco, Mensaje[indice].texto, Mensaje[indice].longitud);
  }
  else
  {
    memcpy(hueco, Mensaje[indice].texto, Mensaje[indice].longitud);
  }
}

void procesaHorario(int indice){
  char* h[3];
  int dia, hora;
  int iPuntero = 1;
  h[0] = Mensaje[indice].texto;
  for(int i=0; Mensaje[indice].texto[i] != 0; i++){
    if(Mensaje[indice].texto[i] == ' '){
      Mensaje[indice].texto[i] = 0;
      h[iPuntero] = &(Mensaje[indice].texto[i+1]);
      iPuntero++;
      if(iPuntero >= 3) break;
    }
  }
  dia = atoi(h[0]);     
  hora = atoi(h[1]);
  if(dia > 5 || dia < 0 || hora > 12 || hora < 0){ return;}
  horario[dia][hora] = strdup(h[2]);
  Serial.printf(" en horario[%d][%d] hay :%s:\n",dia, hora, horario[dia][hora]);
}

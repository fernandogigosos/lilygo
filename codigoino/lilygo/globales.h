#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

//DATOS

//creación del handler de la pantalla
GxIO_Class io(SPI,  EPD_CS, EPD_DC,  EPD_RSET);
GxEPD_Class display(io, EPD_RSET, EPD_BUSY);

BluetoothSerial SerialBT;

int currMen = 0;
int currLec = 0;

mensaje Mensaje[NUM_MENS];
mensaje Saliente[NUM_MENS];

Button button2 = {BOTON2, 0, false};
Button button4 = {BOTON4, 0, false};
Button button3 = {BOTON3, 0, false};
Button button1 = {BOTON1, 0, false};

bool boton1Procesando = false;
bool boton2Procesando = false;
bool boton4Procesando = false;
bool boton3Procesando = false;

TaskHandle_t procesa;

SemaphoreHandle_t xMutex;
SemaphoreHandle_t bMutex;

int intento = 0;
bool primera = true;

Vista actual;
Vista vista1;
Vista vista2;
Vista vista3;
Vista vista4;
Vista vista5;

char* horario[5][12];

char* semana[5] = {"lunes", "martes", "miercoles", "jueves", "viernes"};

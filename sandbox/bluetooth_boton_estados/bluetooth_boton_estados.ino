//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Evndro Copercini - 2018
//
//This example creates a bridge between Serial and Classical Bluetooth (SPP)
//and also demonstrate that SerialBT have the same functionalities of a normal Serial
#define LILYGO_T5_V213
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif


#include <Adafruit_GFX.h>
#include <boards.h>
#include <GxEPD.h>
#include <GxGDEM0213B74/GxGDEM0213B74.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

#include GxEPD_BitmapExamples
#include <Fonts/FreeMonoBold9pt7b.h>


BluetoothSerial SerialBT;

GxIO_Class io(SPI,  EPD_CS, EPD_DC,  EPD_RSET);
GxEPD_Class display(io, EPD_RSET, EPD_BUSY);

const int LEDPIN = 22; 
const int PushButton1 = 15;
int estado = LOW;

void setup() {
  
  SerialBT.begin("ESP32test"); //Bluetooth device name

  pinMode(LEDPIN, OUTPUT);
  pinMode(PushButton1, INPUT);
  
  display.init();
  display.fillScreen(GxEPD_WHITE);
  display.drawExampleBitmap(BitmapExample2, sizeof(BitmapExample1), GxEPD::bm_default | GxEPD::bm_partial_update);

  delay(100);
  display.fillScreen(GxEPD_WHITE);
  display.update();
  escribirPantalla("ESP32test visible");
  display.fillRect(0, display.height(), display.width(), display.height(), GxEPD_WHITE);
  
}

void loop() {
  if ( digitalRead(PushButton1) != estado){ 
    estado = (estado == LOW ? HIGH:LOW);
    digitalWrite(LEDPIN, estado); 
       
    static const char* mens1 = "botón pulsado\n";
    static const char* mens2 = "botón levantado\n";
    
    const char*  mens = (estado == LOW ? mens2 : mens1);
    
    SerialBT.write((const uint8_t*)mens, strlen(mens));
        
  }
  
  if (SerialBT.available()) {
    static char mens[1024];
    leerBT(mens, sizeof(mens));

    escribirPantalla(mens);

  }

}

int leerBT(char* lectura, size_t sz){
  sz--;
  int i = 0;
  for(; SerialBT.available() && i < sz; i++){   
      lectura[i] = SerialBT.read();     
  }
  lectura[i] = 0;
  
  return i;
}

int leer(char* lectura, size_t sz){
  sz--;
  int i = 0;
  for(; Serial.available() && i < sz; i++){   
      lectura[i] = Serial.read();     
  }
  lectura[i] = 0;
  
  return i;
}

void escribirPantalla(char* mensaje){

    display.setRotation(1);
    display.fillRect(0, 0, display.width(), display.height(), GxEPD_WHITE);
    display.setTextColor(GxEPD_BLACK);
    display.setFont(&FreeMonoBold9pt7b);
    display.setCursor(0, display.height()/2);
    
    display.print(mensaje);
    display.updateWindow(0, 0, display.width(), display.height(), true);
    //display.fillRect(0, 0, display.width(), display.height(), GxEPD_WHITE);
}

int leerBT(int iM){
  char lectura[SIZE_BUF];
  int i = 0;
  for(int t = 0; t < ESPERA; t++){
    for(; SerialBT.available() && '\n' != lectura[i-1] && i < SIZE_BUF; i++){   //leemos el mensaje
      lectura[i] = SerialBT.read();     
    }
    if(lectura[i-1] == '\n'){
      break;
    }
    delay(5);
  }
  int c = 0;
  for(int t = 0; t < ESPERA; t++){ 
    if(SerialBT.available()){
      c = SerialBT.read();
    }
    if(c == EOT) break;
    delay(5);
  }
  if(c != 0 && c != EOT) return -5; //no ha llegado EOT
  
  if(0 == i) return -1; //no ha llegado nada
  if(lectura[i-1] != '\n') return -1; //o ha llegado demasiado
  lectura[i] = 0;
  Serial.printf(":%s:\n", lectura);
  char lec[4];
  int longitud;
  char clave;
  int count = 0;
//LEEMOS LA CANTIDAD DE CARÁCTERES DEL MENSAJE, CODIFICADOS EN ASCII  
  for(; count < 3 && lectura[count] != ' '; count++){
    lec[count] = lectura[count];
  }
  if(count > 2 || lectura[count] != ' '){
    return -2; //el campo del tamaño del mensaje es incorrecto
  }
  lec[count] = 0;
  longitud = atoi(lec);
  Serial.printf("tamaño del mensaje :%s:%d\n",lec, longitud);
  if(longitud > 256) return -2;
  count++;
//LEEMOS LA CLAVE

  clave = lectura[count];
  count++;
  if(clave < 65 || clave > 122 || lectura[count] != ' '){
    return -3; //la clave es incorrecta
  }  
  Mensaje[iM].clave = clave;
  
//LEEMOS EL TEXTO
  
  count++;
  int inicio = count;  
  for(; lectura[count] != '\n' && lectura[count] != 13; count++){
    Serial.printf("%d , %d : %d\n",inicio, count, lectura[count]);
    Mensaje[iM].texto[count-inicio] = lectura[count];      
  }  
  if(count-inicio != longitud){
    Serial.printf("%d , %d\n",inicio, count);
    return -4; //la longitud descrita y la real del mensaje no coinciden
  }
  Mensaje[iM].texto[count-inicio] = 0;
  Mensaje[iM].longitud = sizeof(Mensaje[iM].texto);
  return count;
}

//GESTOR DE COMUNICACIÓN BT

bool escribirBT(int iM) {
  static char buf[SIZE_BUF]; //digitos mas el blanco, clave mas el blanco, mensaje y retorno de carro 
  Serial.println("entro en escribirBT");
  
  //solicita la linea
  SerialBT.write(RTS);
  SerialBT.flush();
  Serial.println("HE ENVIADO UN RTS");
  
  //espera el CTS
  int c;
  for(int t = 0; t<ESPERA; t++){
    c = SerialBT.read();    
    if(c < 0) 
      delay(5);
    else if(CTS == c){
      Serial.println(c);
      mensaje* M = &Mensaje[iM];
      int sz = sprintf(buf,"%d %c %s\n", strlen(M->texto), M->clave, M->texto); //digitos mas blanco mas la clave mas el blanco mas texto mas el retorno de carro (y en buf se pone al final un \0)
      Serial.write((const uint8_t*)buf, sz);   
      int nb = SerialBT.write((const uint8_t*)buf, sz);
      SerialBT.flush();
      Serial.printf("HE ENVIADO ESTOS BYTES : %d\n", nb);
      if(nb != sz){
        return false;      
      }
      Serial.printf("envio EOT\n");
      SerialBT.write(EOT);
      SerialBT.flush();
      for(int s = 0; s<ESPERA; s++){
        c = SerialBT.read();
        if(c < 0){ 
          delay(10);
         } else if(BELL == c){
              Serial.println(c);
              return true;
         } else {
              Serial.println(c);
         }
      }
      Serial.printf("timeout\n");
      return false;
    } else {
      Serial.println(c);  
    }
  }
  Serial.printf("timeout\n");
  //No ha logrado comunicarse
  return false;
}

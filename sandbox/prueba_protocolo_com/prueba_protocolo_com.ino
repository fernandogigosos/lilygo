#include "BluetoothSerial.h"
#include "macros.h"
#include "globales.h"
#include "comunicacion.h"

//GESTOR DE BOTÓN

void IRAM_ATTR boton1Pulsado()
{
  button1.numberKeyPresses += 1;
  button1.pressed = true;
  digitalWrite(LEDVERDE, HIGH);
  Serial.printf("Boton1 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button1.numberKeyPresses);
  intento = 0;
}
void IRAM_ATTR boton2Pulsado()
{
  button2.numberKeyPresses += 1;
  button2.pressed = true;
  Serial.printf("Boton1 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button2.numberKeyPresses);
  intento = 0;
}
void IRAM_ATTR boton3Pulsado()
{
  button3.numberKeyPresses += 1;
  button3.pressed = true;
  Serial.printf("Boton1 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button3.numberKeyPresses);
  intento = 0;
}
void IRAM_ATTR boton4Pulsado()
{
  button4.numberKeyPresses += 1;
  button4.pressed = true;
  Serial.printf("Boton1 pulsado (core = %d) : %d veces\n", xPortGetCoreID(), button4.numberKeyPresses);
  intento = 0;
}

void setup()
{
  Serial.begin(115200);

  //BT CONFIGURATION
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("BT active");
  Serial.println(xPortGetCoreID());

  pinMode(button1.PIN, INPUT);

  pinMode(LEDROJO, OUTPUT);
  pinMode(LEDVERDE, OUTPUT);

  attachInterrupt(button1.PIN, boton1Pulsado, RISING);

  xMutex = xSemaphoreCreateMutex();
  xTaskCreatePinnedToCore(
      ProcesaMensaje,   /* Task function. */
      "ProcesaMensaje", /* name of task. */
      10000,            /* Stack size of task */
      NULL,             /* parameter of the task */
      5,                /* priority of the task */
      &procesa,         /* Task handle to keep track of created task */
      0);               /* pin task to core 0 */
}

void loop()
{

  if (button1.pressed)
  {
    Serial.printf("loop (core = %d)\n", xPortGetCoreID());
    Mensaje[0].clave = 'A';
    sprintf(Mensaje[0].texto, "Boton 1 pulsado");
    bool res = escribirBT(0);
    if (res)
    {
      Serial.println("entr'o biennnn el mensajito");
      digitalWrite(LEDVERDE, LOW);
      button1.pressed = false;
      intento = 0;
    }
    else
    {
      Serial.println("entr'o mal, muy mal eh");
      intento++;
    }
  }

  if (SerialBT.available())
  {
    int c;
    c = SerialBT.read();
    if (RTS == c)
    {
      digitalWrite(LEDROJO, HIGH);
      Serial.printf("he recibido un %d : RTS\n", c);
      SerialBT.write(CTS);
      int sz = leerBT(currMen);
      if (sz < 0)
      {
        Serial.printf("Al leer se ha producido un error de tipo: %d\n", sz);
      }
      else
      {
        Serial.printf("lectura correcta\n");
        SerialBT.write(BELL);
        Serial.printf("he recibido:\n\tClave : %c\n\tTexto : %s\n\tTamaño de la cadena : %d\n", Mensaje[currMen].clave, Mensaje[currMen].texto, sz);
        digitalWrite(LEDROJO, LOW);
        xSemaphoreTake(xMutex, portMAX_DELAY);
        currMen = (++currMen) % NUM_MENS;
        xSemaphoreGive(xMutex);
      }
    }
    else
    {
      Serial.printf("he recibido un %d y no sé por qué\n", c);
    }
  }

  if (intento > 10)
  {
    button1.pressed = false;
    digitalWrite(LEDVERDE, LOW);
  }
}

void ProcesaMensaje(void * pvParameters)
{
  Serial.print("ProcesaMensaje running on core ");
  Serial.println(xPortGetCoreID());

  for (;;)
  {
    vTaskDelay(1);
    xSemaphoreTake(xMutex, portMAX_DELAY);
    if (currMen != currLec)
    {
      xSemaphoreGive(xMutex);
      Serial.printf("procesado el mensaje %d, que contiene la clave %c\n", currLec, Mensaje[currLec].clave);
      switch (Mensaje[currLec].clave){
      case ('A'):{
        copiaCampo(vista1.texto1, currLec);
        Serial.printf("contenido de texto1: %s\n", vista1.texto1);
      }break;
      case ('B'):{
        copiaCampo(vista1.texto2, currLec);
        Serial.printf("contenido de texto2: %s\n", vista1.texto2);
      }break;
      case ('z'):{
        if (vista1.texto1 != NULL && vista1.texto2 != NULL){
          Serial.printf("se procesa la vista1:\nDatos:\n");
          Serial.printf("texto1 : %s\ntexto2 : %s\n", vista1.texto1, vista1.texto2);
        }else{
          Serial.printf("faltan datos en la vista1\n");
        }
      }break;
      default:{
        Serial.printf("esto que es: %c\n", Mensaje[currLec].clave);
      }
      }
      currLec = (++currLec) % NUM_MENS;
    }
    xSemaphoreGive(xMutex);
  }
}

void copiaCampo(char* &hueco, int indice)
{
  if (hueco == NULL || (sizeof(hueco) <= Mensaje[indice].longitud))
  {
    hueco = (char *)malloc(Mensaje[indice].longitud);
    memcpy(hueco, Mensaje[indice].texto, Mensaje[indice].longitud);
  }
  else
  {
    memcpy(hueco, Mensaje[indice].texto, Mensaje[indice].longitud);
  }
}

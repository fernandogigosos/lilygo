struct Button {
  const uint8_t PIN;
  uint32_t numberKeyPresses;
  bool pressed;
};

TaskHandle_t Task1;
Button button1 = {15, 0, false};

void IRAM_ATTR isr() {
  button1.numberKeyPresses += 1;
  Serial.println(xPortGetCoreID());
  button1.pressed = true;
}

void setup() {
  Serial.begin(115200);

    xTaskCreatePinnedToCore(
                    Task1code,   /* Task function. */
                    "Task1",     /* name of task. */
                    10000,       /* Stack size of task */
                    NULL,        /* parameter of the task */
                    1,           /* priority of the task */
                    &Task1,      /* Task handle to keep track of created task */
                    0);          /* pin task to core 0 */  
                    
  pinMode(button1.PIN, INPUT);
  attachInterrupt(button1.PIN, isr, RISING);
}

//Task1code: blinks an LED every 1000 ms
void Task1code( void * pvParameters ){
  Serial.print("Task1 running on core ");
  Serial.println(xPortGetCoreID());

  for(;;){
    Serial.println("hola 1");
    delay(1000);

  } 
}

void loop() {
  if (button1.pressed) {

      Serial.printf("Button 1 has been pressed %u times in core %d\n", button1.numberKeyPresses, xPortGetCoreID());
      button1.pressed = false;
  }

  //Detach Interrupt after 1 Minute
  static uint32_t lastMillis = 0;
  if (millis() - lastMillis > 60000) {
    lastMillis = millis();
    detachInterrupt(button1.PIN);
  Serial.println("Interrupt Detached!");
  }
}
